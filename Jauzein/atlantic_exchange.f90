! SEE MODEL DANIEL GARCIA-CASTELLANOS


program create_gibraltar_props
implicit none

integer :: time
real :: flux_in, flux_out

! West-Atl exchange values
! Meijer 2006, p476
!
flux_in = 3e12              !m3/y
flux_out = 0.0
!flux_out = 0.015*flux_in    !m3/y

open(101,FILE='atlantic.flux')

    do time = 1, 10000

        write(101,*), flux_in, flux_out, time

    end do

close(101)

print*, "Atlantic Exchange values created"
end program
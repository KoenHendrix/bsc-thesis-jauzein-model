set term postscript eps color
set xlabel "Time"
set ylabel "Volume"
set xrange [ 0 : 20000 ]
set yrange [ -2000 : 0 ]
set output "plot_time_volume.eps"
splot "salinity_over_time_simple.dat" title "Salinity over Time"
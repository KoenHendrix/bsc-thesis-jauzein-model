program sea_level_over_time

! Determines the sea level over time
! based on different factors such as
! evaporation, fresh water input.
! Straight of Sicily is regarded as
! 300m deep.
!
! Koen Hendrix, Sept 10, 2013 

implicit none

real, allocatable, dimension(:) :: interpolation(:)
real, allocatable, dimension(:) :: interpolation_east(:)
real, allocatable, dimension(:) :: interpolation_west(:)
real, allocatable, dimension(:) :: interpolation_volume(:)
real, allocatable, dimension(:) :: interpolation_volume_east(:)
real, allocatable, dimension(:) :: interpolation_volume_west(:)
integer :: i, j, d, time, secondsyear, max_depth, neg_points
integer :: topogrid_x, topogrid_y
real :: depth, dot, sealevel, evaporation
real :: river_input, river_input_east, river_input_west
real :: ebro, rhone, po, nile, blacksea
real :: func
real :: salinity_atlantic, salinity_med_sea, salinity_rivers
real :: volume_over_time, sot
real :: max_volume_start
real :: phic, phic0, s0, S_ca, river_factor, evap, river_factor1, preciptation, prec, r0
real :: atl_flux_in, atl_salt_in
external :: func

secondsyear = 31557600

open(11,FILE='research_area.in')
 read(11,*) topogrid_x, topogrid_y
 read(11,*) max_depth
close(11)

open(12, file='river_input.in')
 read(12,*) ebro
 read(12,*) rhone
 read(12,*) po
 read(12,*) nile
 read(12,*) blacksea
close(12)

open(13,FILE='salt_properties.in')
 read(13,*) salinity_med_sea
 read(13,*) salinity_atlantic
 read(13,*) salinity_rivers
close(13)

open(14, file='volume_data.dat')
 read(14,*) max_volume_start
close(14)

open(15, file='hypsometry_data.dat')
 read(15,*) s0
close(15)

!##################
!##################
S_ca = 2.0 * s0   ! Needs refinement! Surface _ Catchment Area
!##################
!##################

open(131, file='hypsometry_data.dat')
open(132, file='hypsometry_data_east.dat')
open(133, file='hypsometry_data_west.dat')
open(141, file='volume_data.dat')
open(142, file='volume_data_east.dat')
open(143, file='volume_data_west.dat')

allocate (interpolation(max_depth))
allocate (interpolation_east(max_depth))
allocate (interpolation_west(max_depth))
allocate (interpolation_volume(max_depth))
allocate (interpolation_volume_east(max_depth))
allocate (interpolation_volume_west(max_depth))
        do d=1, max_depth
          read(131,*) interpolation(d), depth
          read(132,*) interpolation_east(d), depth
          read(133,*) interpolation_west(d), depth
          read(141,*) interpolation_volume(d), depth
          read(142,*) interpolation_volume_east(d), depth
          read(143,*) interpolation_volume_west(d), depth
        end do

river_input = ((2.5*(ebro+rhone+po+nile+blacksea))*secondsyear)
river_input_east = ((ebro+rhone)*secondsyear)
river_input_west = ((po+nile+blacksea)*secondsyear)

open(21, file='sealevel_over_time_simple.dat')
open(22, file='seavolume_over_time_simple.dat')
open(23, file='salinity_over_time_simple.dat')
open(24, file='s_ratio_vs_chi.dat')
open(25, file='evap_salinity.dat')

! surface_t    time      sot
! 499078.906   7545   704.868225  -- Na 0.8m evap per jaar
! 725001.562  12611   398.949066  -- Na phic/phic0
! 943275.938  18479   203.993439  -- Na river_factor
! 943276.062  18482   203.993362  -- Na Preciptation

sealevel = 00.0 !
volume_over_time = 0.0
sot = 35.0
atl_salt_in = 0.0
neg_points = 0

r0 = 0.5
evaporation = 2.0
preciptation = 0.5

    do time = 0,10000


!Reken een geval uit en kijk of de kleine s0 gelijk is aan interpolatiefunctie
!Uitgecommenteerde functies zijn van formule 1 (Jauzein). Actieve functies zijn van de functie op een pagina eerder.


                    !##### Evaporation
                        phic = (1-(0.32/11.0)*(sot/35.))  ! phi c
                        phic0 = (1-(0.32/11.0))          ! phi 0
!                        phic = 1.0
!                        phic0 = 1.0
                     evap = evaporation * (phic/phic0) * ( func(sealevel,interpolation,max_depth) / s0)
!                    evap = evaporation * ( func(sealevel,interpolation,max_depth) / s0)
 !                   evap = evaporation * (phic/(phic0*s0)) * func(sealevel,interpolation,max_depth)
                    !#####
                    !##### River recharge
                        river_factor1 = (river_input*(((S_ca - func(sealevel,interpolation,max_depth)) / (S_ca - s0))))
                    river_factor = river_factor1 / func(sealevel,interpolation,max_depth)*1E-6
!                    river_factor = R0 * (S_ca/(S_ca-s0))
!                     river_factor = 2.0 * R0
                    !#####
                    !##### Preciptation
                    prec = (preciptation * func(sealevel,interpolation,max_depth) /s0)
!                    prec = ((preciptation/s0) - (R0/(S_ca-s0))) * func(sealevel,interpolation,max_depth)
                    !#####
                    !##### Atlantic Influx
                    atl_flux_in = 0.50
                    atl_salt_in = atl_salt_in + ((atl_flux_in * 1000.0)*35.0)
                    !#####


            dot = evap - river_factor - prec - atl_flux_in
            sealevel = sealevel + dot
            volume_over_time = func(sealevel,interpolation_volume,max_depth)
            sot = ((salinity_med_sea * max_volume_start)+atl_salt_in) / volume_over_time !salinity over time


IF (sealevel .lt. 0.0) then
print*, "ERROR :: Sea level above ZERO in year", time, "Stopping calculations."
stop
end if

    write (21,*) time, -sealevel
    write (22,*) time, volume_over_time
    write (23,*) time, sot
    write (24,*) phic, ( func(sealevel,interpolation,max_depth) / s0)
    write (25,*) evap, sot

    end do
close(21)
close(22)
close(23)
close(24)

!print*, neg_points

!--- begin raw code ideas
!evaporation = variable with temperature AND salinity
!for east and west subbasins, use same formula, but sealevel will then be 300. T for split needs to be determined properly first!
! if sea level > 300 then use main , else if use west and east
! consider different levels between east and west! If level west is 100 and east is 350 then east will flow into west!!
! open sealevel_east, sealevel_west, sealevel_global
! do time = 0, 1000000 !(1MA)
!Salinity levels are fictional!
!do time = 1, 1000000
!       evaporation = (0.8 + (0.05*(depth-1)) - (salinity(time-1)*0.10))
!      sea_level_over_time = sea_level_over_time + evaporation - river_input
! Volume must be checked with volume_data files every iteration to recalculate salinity for the next step.
!IF net_atlantic_input .gt. 0
!    then salt(time) = (salt(time-1)) + river_input + (net_atlantic_input * salinity_atlantic)
!ELSE IF net_atlantic_input .lt. 0
!    then salt(time) = (salt(time-1)) + river_input + (net_atlantic_output * (salinity(time-1))
!end IF


!-- end of raw code ideas---
close(131)
close(132)
close(133)

deallocate(interpolation)
deallocate(interpolation_west)
deallocate(interpolation_east)
deallocate(interpolation_volume)
deallocate(interpolation_volume_east)
deallocate(interpolation_volume_west)


print *, 'sea_level_over_time.f90 done'
end program

!--------------------------------------------------------------------------------------------

real function func(depth,interpolation,max_depth)
implicit none
integer :: l1, l2, max_depth
real, dimension(max_depth) :: interpolation
real :: depth

l1=int(depth)+1
l2=int(depth)+2

func=interpolation(l1)-(depth-int(depth))*(interpolation(l1)-interpolation(l2))

end function


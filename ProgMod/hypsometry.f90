program hypsometry

! Calculates the hypsometry of the Mediterannean sea as we
! know it today. The input file is topo.z, the variables
! are read from research_area.in. Constants pi and 
! radius_earth are hardcoded.
! 
! Koen Hendrix, Sept 10, 2013 

implicit none

integer :: topogrid_x, topogrid_y
integer :: i, j, depth
integer :: max_depth, resolution_sec
real, allocatable, dimension (:,:) :: topogrid
real, allocatable, dimension (:,:) :: westgrid
real, allocatable, dimension (:,:) :: eastgrid
real, allocatable, dimension (:,:) :: area_var_grid
real :: lat_av, radius_earth, pi, resolution_degr, lat_edge_east
real :: hypsometry_data, hypsometry_data_east, hypsometry_data_west

open(11,FILE='research_area.in')
  read(11,*) topogrid_x, topogrid_y
  read(11,*) max_depth
  read(11,*) lat_edge_east
  read(11,*) resolution_sec
close(11)

    radius_earth = 6371.0
    pi = 4.*atan(1.)
    resolution_degr = resolution_sec / 60.

open(101,FILE='topo.z')
open(111,FILE='topo_basin_west.z')
open(112,FILE='topo_basin_east.z')

allocate(topogrid(topogrid_x,topogrid_y))
allocate(westgrid(topogrid_x,topogrid_y))
allocate(eastgrid(topogrid_x,topogrid_y))
    do j=1, topogrid_y
    do i=1, topogrid_x
            read(101,*) topogrid(i,j)
            read(111,*) westgrid(i,j)
            read(112,*) eastgrid(i,j)
    end do
    end do
close(101)
close(111)
close(112)

allocate(area_var_grid(topogrid_x,topogrid_y))
    do j=1, topogrid_y
    do i=1, topogrid_x
            if (topogrid(i,j) .le. 0) then
            lat_av = lat_edge_east - (j-1)*resolution_degr - (resolution_degr/2.0)
            area_var_grid(i,j) = ((2.0*pi*radius_earth/360.0 * resolution_degr)**2) * cos(pi/180.0*lat_av)
            end if
    end do
    end do


open(201,FILE='hypsometry_data.dat')
open(202,FILE='hypsometry_data_east.dat')
open(203,FILE='hypsometry_data_west.dat')
    do depth=0, max_depth
        hypsometry_data = 0.0
        hypsometry_data_east = 0.0
        hypsometry_data_west = 0.0
            do j=1, topogrid_y
            do i=1, topogrid_x
                    if (topogrid(i,j) < -depth) then
                    hypsometry_data = hypsometry_data + area_var_grid(i,j)
                    end if

                    if (eastgrid(i,j) < -depth) then
                    hypsometry_data_east = hypsometry_data_east + area_var_grid(i,j)
                    end if

                    if (westgrid(i,j) < -depth) then
                    hypsometry_data_west = hypsometry_data_west + area_var_grid(i,j)
                    end if
            end do
            end do
        write(201,*) hypsometry_data, depth
        write(202,*) hypsometry_data_east, depth
        write(203,*) hypsometry_data_west, depth
    end do
close(201)
close(202)
close(203)

print *, 'hypsometry.f90 done'
end program
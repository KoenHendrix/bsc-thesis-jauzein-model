program basin_volume

! Calculates the volume per depth
! per sub basin and grand basin. 
!  
! Koen Hendrix, Sept 10, 2013

implicit none

integer :: topogrid_x, topogrid_y
integer :: depth, max_depth, d, i
real :: hypsometry_data, hypsometry_data_east, hypsometry_data_west
real :: volume_total, volume_total_east, volume_total_west
real :: volume_total_total, volume_total_total_west, volume_total_total_east

open(11,FILE='research_area.in')
 read(11,*) topogrid_x, topogrid_y
 read(11,*) max_depth
close(11)

open(201,FILE='hypsometry_data.dat')
open(202,FILE='hypsometry_data_east.dat')
open(203,FILE='hypsometry_data_west.dat')

!This is a dirty workaround to store the data from top to bottom instead of from bottom to top.
    do i = 1, max_depth
        read(201,*), hypsometry_data, depth
        read(202,*), hypsometry_data_east, depth
        read(203,*), hypsometry_data_west, depth
        volume_total_total = volume_total_total + hypsometry_data
        volume_total_total_east = volume_total_total_east + hypsometry_data_east
        volume_total_total_west = volume_total_total_west + hypsometry_data_west
    end do

close(201)
close(202)
close(203)

open(201,FILE='hypsometry_data.dat')
open(202,FILE='hypsometry_data_east.dat')
open(203,FILE='hypsometry_data_west.dat')
open(204,FILE='volume_data.dat')
open(205,FILE='volume_data_east.dat')
open(206,FILE='volume_data_west.dat')

volume_total = 0.0
volume_total_east = 0.0
volume_total_west = 0.0

    do d = 1, max_depth
        read(201,*), hypsometry_data, depth
        read(202,*), hypsometry_data_east, depth
        read(203,*), hypsometry_data_west, depth
            volume_total = volume_total + hypsometry_data
            volume_total_east = volume_total_east + hypsometry_data_east
            volume_total_west = volume_total_west + hypsometry_data_west
        write(204,*), volume_total_total-volume_total, d
        write(205,*), volume_total_total_east-volume_total_east, d
        write(206,*), volume_total_total_west-volume_total_west, d
    end do

close(201)
close(202)
close(203)
close(204)
close(205)
close(206)

print *, 'basin_volume.f90 done'

end program
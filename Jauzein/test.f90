! Recreation of the model for the Messinian Salinity Crisis
! by Jauzein & Hubert (1984). This model is explained in
! their paper "LES BASSINS OSCILLANTS: UN MODÈLE DE
! GENÈSE DES SÉRIES SALINES".
!
! Model by: Koen Hendrix [Bachelor Thesis]
! Supervision by: Paul Meijer
!
! $DATE

program Jauzein
implicit none

real    :: volume_previous, f_v, evap, leftover, phic, phic0, R_term, c_over_time1, c_over_time, v_false, dvdt, S, dcdt, v_time
real    :: I, c_input, s_time, dvdt2
real    :: v, h, average_depth
real    :: E0, P0, R0
real    :: S_catch_area, S0, s_area_current
real    :: c_time
real    :: v_over_time, water_level
integer :: time
integer :: preciptation

E0 = 2.0    !m/y
P0 = 0.5    !m/y
R0 = 0.5    !m/y

S = 2*10e6  !km2
s0 = 10e6   !km2

average_depth = 2.0 !km
I = 0.99 !m                   !(32000.*(365.25*24.*3600.))/1e9 !km3
c_input = 1.0 ! index for ~35 g/l

h = 1.0 !km
v = 10e6 !km2


s=s0*h/h0
v=h/3


! ---------------------- Start Jauzein Model ----------------------- !

s_time = 10e6  !km2       !t = 0
c_time = 1.0   !index     !t = 0
v_time = 10e6  !km3       !t = 0

!#############################################
do time = 1, 100000
!#############################################

phic = (1-(0.32/11.0)*c_time)   !temp value
phic0 = (1-(0.32/11.0))
f_v = v_time * (s_time/(sqrt(v*v_time)))

dvdt = I + (R0 * (S/(S-s0))) + (((P0/s0) - (R0/(S-s0))) * f_v) - (E0 * (phic/(phic0*s0)) * f_v )
dcdt = (I * (c_input/(1000+dvdt))) - ((c_time/v_time) * dvdt)

v_time = v_time + (dvdt*s_time)
c_time = c_time + dcdt
s_time = v_time/(0.5*average_depth)

print*, time, c_time*35.0, v_time, dcdt, dvdt
end do
end program
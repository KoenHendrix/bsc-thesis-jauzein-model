program create_sub_area

! Determines the sub basins for east mediterannean
! and west mediterannean sea, divided by the 
! Straight of Sicily (300m depth). This is done
! by using mask.z on top of topo.z.
!  
! Koen Hendrix, Sept 9, 2013

implicit none

integer :: topogrid_x, topogrid_y
integer :: i, j
real, allocatable, dimension (:,:) :: topogrid
real, allocatable, dimension (:,:) :: maskgrid

open(11,FILE='research_area.in')
  read(11,*) topogrid_x, topogrid_y
close(11)

open(101,FILE='topo.z')
open(102,FILE='mask.z')
open(211,FILE='topo_basin_west.z')
open(212,FILE='topo_basin_east.z')
allocate(topogrid(topogrid_x,topogrid_y))
allocate(maskgrid(topogrid_x,topogrid_y))
    do j=1, topogrid_y
    do i=1, topogrid_x
            read(101,*) topogrid(i,j)
            read(102,*) maskgrid(i,j)
                    if (maskgrid(i,j) == 1) then
                    write(211,*) topogrid(i,j)
                    write(212,*) 100
                    else if (maskgrid(i,j) == 2) then
                    write(211,*) 100
                    write(212,*) topogrid(i,j)
                    end if
    end do
    end do

close(101)
close(202)
close(211)
close(212)

print *, 'create_sub_area.f90 done'
end program
program msc_main
implicit none

call system("gfortran create_sub_area.f90")
call system("./a.out")

call system("gfortran hypsometry.f90")
call system("./a.out")

call system("gfortran basin_volume.f90")
call system("./a.out")

print *, "Start calculating sea_level_over_time.f90"
call system("gfortran sea_level_over_time.f90")
call system("./a.out")

    print*, "Start plotting time_salinity"
call system("gnuplot time_salinity")
!call system("open plot_salinity_over_time.eps")

    print*, "Start plotting time_sealevel"
call system("gnuplot time_sealevel")
!call system("open plot_sealevel_over_time.eps")

call system("gnuplot evap_salinity")
print*, "plotting evap_salinity"

end program